var mainSeq
var graphline
var fx


function setup() {
  galaxySetup()
  galaxySyncStart()
  onGalaxyReady = () => {
    colorMode(HSB)
    strokeWeight(6)
    mainSeq = new arithSeq(1, 1)
    graphline = new Trail(0, 0, 0, 0, realWidth)
    fx = new Superimposer()
    fx.addF((t) => {return 100*sin(PI*t/(3600))})
    fx.addF((t) => {return 50*cos(PI*t/(1800))})
    fx.addF((t) => {return 40*sin(PI*t/(500)+ 20)})
  }
}


function draw() {
  if (galaxyReady) {
    let nextX = (startTime-getUnixMilli())/10
    background(0)
    translate(xOffset - nextX, yOffset + realHeight/2)
    let c = color(360*abs(sin(PI*getUnixMilli()/3600)), 100, 100)
    graphline.draw()
    graphline.addPoint(nextX, fx.compute(getUnixMilli()), c)
  }
}


// Classes
class Superimposer {
  constructor() {
    this.functions = []
  }

  addF(f) {
    this.functions.push(f)
  }

  compute(x) {
    let sum = 0
    for (let i = 0; i < this.functions.length; i++) {
      sum += this.functions[i](x)
    }
    return sum
  }
}


class arithSeq {
  constructor(start, diff, n=0) {
    this.a = start
    this.d = diff
    this.n = n
  }

  tn(n=this.n) {
    return this.a + this.d * (n - 1)
  }

  sn(n=this.n) {
    return (n/2) * (this.a + this.tn(n))
  }

  next() {
    let val = this.tn()
    this.n++
    return val
  }
}


class Trail {
  constructor(x1, y1, x2, y2, max, col1=0, col2=0) {
    this.points = [[x1, y1, col1], [x2, y2, col2]]
    this.maxlen = max
  }

  addPoint(x, y, col=255) {
    this.points.push([x, y, col])
    if (this.points.length > this.maxlen) {
      this.points = this.points.slice(1)
    }
  }

  draw() {
    let prevPoint = this.points[0]
    for (let i = 1; i < this.points.length; i++) {
      stroke(prevPoint[2])
      strokeWeight(2)
      line(prevPoint[0], prevPoint[1], this.points[i][0], this.points[i][1])
      prevPoint = this.points[i]
    }
  }
}
