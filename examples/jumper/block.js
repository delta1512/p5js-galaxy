class Block {
  constructor(x, y, l, h) {
    this.x = x
    this.y = y
    this.l = l
    this.h = h
    this.prevTime = getUnixMilli()
  }

  draw() {
    let tmp = getUnixMilli()
    this.y += (tmp-this.prevTime)/10
    this.prevTime = tmp
    if (this.y > realHeight) {
      // Flag that this block is ready for garbage collection
      return true
    }
    fill('#4EE83C')
    noStroke()
    rect(this.x, this.y, this.l, this.h)
    return false
  }
}
