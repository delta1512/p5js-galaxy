const BLOCK_HEIGHT = 50
const MIN_LEN = 175
const THICKNESS = 50

var player
var blocks = []
var playersDead = []


function setup() {
  galaxySetup()
  galaxySyncStart()
  onGalaxyReady = () => {
    player = new Player(width/4, height/4)
    for (let i = 0; i < realHeight; i+=200) {
      blocks.push(new Block(random(width/2), i, random(MIN_LEN, width/2), THICKNESS))
      blocks.push(new Block(random(width/2), i, random(MIN_LEN, width/2), THICKNESS))
    }
  }
}


function draw() {
  if (galaxyReady) {
    background(131, 252, 255)

    player.draw()
    for (let i = 0; i < blocks.length; i++) {
      if (blocks[i].draw()) {
        blocks[i] = new Block(random(width), -THICKNESS, random(MIN_LEN, width/2), THICKNESS)
      }
    }

    checkMovement()

    if (player.dead) {
      playerLose()
    } else if (playersDead.length == totalScreens-1) {
      playerWin()
    }
  }
}


function keyPressed() {
  if (keyCode == UP_ARROW) {
    player.jump()
  }
}


function checkMovement() {
  if (keyIsDown(LEFT_ARROW)) {
    player.move(LEFT)
  } else if (keyIsDown(RIGHT_ARROW)) {
    player.move(RIGHT)
  }
}


function onRelayMsg(event) {
  let data = JSON.parse(event.data)
  if (data.death >= 0 && data.death < totalScreens && !(data.death in playersDead)) {
    playersDead.push(data.death)
  }
}


function playerWin() {
  noLoop()
  fill(255, 150)
  rect(0, 0, width, height)
  stroke(0)
  strokeWeight(3)
  textSize(78)
  fill(255)
  text("YOU WIN!!!", width/2-175, height/2)
}


function playerLose() {
  noLoop()
  fill(20, 150)
  rect(0, 0, width, height)
  stroke(0)
  strokeWeight(3)
  textSize(78)
  fill(255)
  text("YOU DIED", width/2-175, height/2)
  galaxyBroadcast({'death' : displayID})
}
