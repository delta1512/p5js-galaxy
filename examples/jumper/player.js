const GRAVITY = 1
const JUMP_V = -25
const LEFT = -10
const RIGHT = -LEFT


class Player {
  constructor(xi, yi) {
    this.x = xi
    this.y = yi
    this.vx = 0
    this.vy = 0
    this.w = 100
    this.h = 100
    this.jumps = 2
    this.dead = false
    this.img = loadImage('https://i.imgur.com/puGxNAT.png')
  }


  jump() {
    if (this.jumps-- > 0) {
      this.vy = JUMP_V
    }
  }


  move(v) {
    this.vx += v
  }


  isOnScreenEdgex() {
    return this.x < 0 || this.x > width
  }


  isOnScreenEdgey() {
    return isOnScreenEdgeYtop() || isOnScreenEdgeYbot()
  }


  isOnScreenEdgeYtop() {
    return this.y < 0
  }


  isOnScreenEdgeYbot() {
    return this.y > height
  }


  intersects(b) {
    return (this.x > b.x && this.x < b.x+b.l) &&
            (this.y > b.y && this.y < b.y+b.h)
  }


  isOnBlock() {
    for (let i = 0; i < blocks.length; i++) {
      let cBlock = blocks[i]
      if (this.intersects(cBlock)) {
        return i+1
      }
    }
    return false
  }


  draw() {
    let prevx = this.x
    let prevy = this.y
    this.x += this.vx
    this.y += this.vy
    let b = this.isOnBlock()

    if (b) {
      let cb = blocks[b-1]
      this.vy = 0
      this.x = prevx
      if (cb.y < prevy) {
        this.y = prevy + 5
      } else {
        this.y = cb.y
        this.jumps = 2
      }
    } else if (this.isOnScreenEdgex()) {
      this.x = prevx
    } else if (this.isOnScreenEdgeYbot()) {
      this.dead = true
    } else if (this.isOnScreenEdgeYtop()) {
      this.vy = 0
      this.y = prevy
    }

    fill(0, 255, 0)
    image(this.img, this.x-this.w/2, this.y-this.w)

    this.vx = 0
    this.vy += GRAVITY
  }
}
