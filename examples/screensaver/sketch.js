var bouncer

function setup() {
  galaxySetup()
  galaxySyncStart()
  onGalaxyReady = () => {
    let v = 7
    bouncer = new Bouncer(realWidth/2, realHeight/2, v, v, d=100)
  }
}


function draw() {
  if (galaxyReady) {
    translate(xOffset, yOffset)
    background(30)

    bouncer.draw()
    galaxyDebug()
  }
}


class Bouncer {
  constructor(x=realWidth/2, y=realHeight/2, vx=0, vy=0, d=20) {
    this.x = x
    this.y = y
    this.vx = vx
    this.vy = vy
    this.d = d
    this.stroke = [0, 0, 0]
    this.fill = [27, 200, 10]
  }

  draw() {
    this.x += this.vx
    this.y += this.vy

    // Bounds checking to ensure the Bouncer bounces off walls
    // +this.d because the square is drawn from the top left
    if ((this.x < 0) || (this.x+(this.d) > realWidth)) {
      this.vx = this.vx*-1
      this.getNewColours()
    }
    if ((this.y < 0) || (this.y+(this.d) > realHeight)) {
      this.vy = this.vy*-1
      this.getNewColours()
    }

    fill(this.fill)
    stroke(this.stroke)
    strokeWeight(0)
    square(this.x, this.y, this.d)
  }

  getNewColours() {

  }
}
