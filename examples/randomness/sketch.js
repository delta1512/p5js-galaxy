var n = 0


function setup() {
  galaxySetup()
  onGalaxyReady = () => {
    background(0)
    fill(255)
    noStroke()
    textSize(32)
    frameRate(2)
  }
}


function draw() {
  if (galaxyReady) {
    text(n++, random(width), random(height))
  }
}


function mousePressed() {
  galaxyBroadcast({'done' : true})
  noLoop()
}


function onRelayMsg(event) {
  data = JSON.parse(event.data)
  if (data.done) {
    noLoop()
  }
}
