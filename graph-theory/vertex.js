class Vertex {
  constructor(x=0, y=0, v=[0, 0], r=20) {
    this.x = x      // x-position
    this.y = y      // y-position
    this.v = v      // 2D-velocity
    this.d = r*2    // Diameter of circle
  }


  draw() {
    fill(175)
    stroke(0)
    strokeWeight(3)
    circle(this.x, this.y, this.d)
  }


  doPhysics() {
    this.v[0] = this.v[0]*FRICTION
    this.v[1] = this.v[1]*FRICTION
    this.x = this.x + this.v[0]
    this.y = this.y + this.v[1]

    // Bounds checking to ensure vertices bounce off walls
    if ((this.x < 0) || (this.x > width)) {
      this.v[0] = this.v[0]*-1
    }
    if ((this.y < 0) || (this.y > height)) {
      this.v[1] = this.v[1]*-1
    }
  }
}
