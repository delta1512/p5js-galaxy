class Edge {
  constructor(v1, v2) {
    this.v1 = v1        // vertex 1
    this.v2 = v2        // vertex 2
  }

  draw() {
    stroke(0)
    strokeWeight(4)
    line(this.v1.x, this.v1.y, this.v2.x, this.v2.y)
  }
}
