var vertices = []
var edges = []

const FRICTION = 0.925    // Coefficient of velocity


function setup() {
  createCanvas(windowWidth, windowHeight)
}


function draw() {
  background(30)
  // Process all the vertices
  for (let i = 0; i < vertices.length; i++) {
    let cvertex = vertices[i]
    cvertex.doPhysics()
    cvertex.draw()
  }

  // Process all the edges
  for (let i = 0; i < edges.length; i++) {
    let cedge = edges[i]
    cedge.draw()
  }
}


function mousePressed() {
  addVertex(mouseX, mouseY)

  // Add a random edge
  if ((vertices.length > 1) && (random() > 0.75)) {
    let v1i = round(random(vertices.length-1))
    let v2i = v1i
    // Find another different edge
    while (v2i == v1i) {
      v2i = round(random(vertices.length-1))
    }

    addEdge(vertices[v1i], vertices[v2i])
  }
}


function addVertex(x, y) {
  let v = [random(-15, 15), random(-15, 15)]
  vertices.push(new Vertex(x, y, v))
}


function addEdge(v1, v2) {
  edges.push(new Edge(v1, v2))
}
