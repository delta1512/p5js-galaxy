const MARGIN = 10          // The number of pixels the sketch is moved from the top left
const NET_TIMEOUT = 5*1000 // The time in milliseconds for how long to wait for networking events
const DEBUG_LINES = 200    // The number of lines drawn for debugging
const DEBUG_CIRCLE_D = 200 // Diameter of the debug circles
const DEBUG_LINE_EXT = 1000// The amount of pixels added to the lines that the debug lines intersect
const DEFAULT_RELAY_CALLBACK = function(event) {
  console.log("To use networked features, you need to set onRelayMsg to a function that handles JSON data. Check p5galaxy.js for this example.")
  console.log(event.data)
}


// Vars to be used from outside
var galaxyReady = false   // Flag to tell whether the glaxy library is ready
var startTime = 0         // The time when the sketch started
var xOffset = 0           // xOffset used to translate the scene
var yOffset = 0           // yOffset used to translate the scene
var screenW               // Width of each screen
var screenH               // Height of each screen
var bezel = 0             // Amount of pixels to compensate for the bezel
var realWidth             // The real width used to define the total canvas
var realHeight            // The real height used to define the total canvas
var onRelayMsg            // The callback function for when a message is received over the web socket
var onGalaxyReady         // A function that is called when the setup is completed
var displayID             // A unique ID that identifies this screen
var totalScreens          // Total number of screens
// Vars used internally
var displayNo = [0, 0]    // x,y of the current screen (0, 0) is top left
var synced = false        // Tracks whether the galaxySyncStart() has started the sketch
var netReady = false      // Tracks whether the network configuration has been successfully initiated
var numScreensX = 1       // Total amount of screens along the X-axis
var numScreensY = 1       // Total number of screens along the Y-axis
var wsrEndpoint = ""      // The Websocket relay endpoint for networked functionality
var relay = null          // The Websocket object to handle relays
var seed = null           //  The chosen random seed
var seeds = []            // Temporarily used array to figure out which seed is to be used
var status_msg            // The DOM status message to provide some user feedback
// Debug vars
var debug = false         // Whether debug mode is on or off


// MANDATORY FUNCTION
// Must be placed in setup()
// Do not call createCanvas() in setup(), call this method instead
// Sets up the canvas and other configuration values
function galaxySetup() {
  // Provide user feedback
  console.log("p5-galaxy is starting...")
  status_msg = createP("p5-galaxy is starting...")
  status_msg.style("padding-left", "10px")
  status_msg.style("padding-top", "10px")
  // Process URL args
  let url = new URL(document.URL)
  url = url.searchParams
  screenW = Number(url.get("w"))   || windowWidth
  screenH = Number(url.get("h"))   || windowHeight
  bezel = Number(url.get("bezel")) || bezel
  numScreensX = Number(url.get("screensx")) || numScreensX
  numScreensY = Number(url.get("screensy")) || numScreensY
  totalScreens = numScreensX * numScreensY
  displayNo[0] = Number(url.get("dx")) || displayNo[0]
  displayNo[1] = Number(url.get("dy")) || displayNo[1]
  debug = Boolean(url.get("debug")) || debug
  wsrEndpoint = url.get("endpoint")

  // Setup the endpoint if specified
  if (wsrEndpoint) {
    relay = new WebSocket(wsrEndpoint)
    relay.onopen = relayOpen
    relay.onerror = galaxyNetError
  } else {
    doGalaxyReady()
  }
}


// OPTIONAL FUNCTION
// Placed at the end of draw()
// Displays the debugging graphics over the canvas if debug=true.
function galaxyDebug() {
  if (debug) {
    background(200)

    // Draw lines to test alignment
    let x1 = -realHeight
    let y1 = -realWidth - DEBUG_LINE_EXT
    let x2 = realWidth + realHeight
    let y2 = realHeight - DEBUG_LINE_EXT
    for (let i = 0; i < DEBUG_LINES; i++) {
      let b = i * 100
      stroke(0)
      strokeWeight(4)
      line(x1, y1 + b, x2, y2 + b)
    }

    // Draw circles to test sync and bezel
    noStroke()
    fill(255 * (second() % 2))
    ellipse(0-xOffset,          0-yOffset,             DEBUG_CIRCLE_D)
    ellipse(width-xOffset,      0-yOffset,             DEBUG_CIRCLE_D)
    ellipse(0-xOffset,          height-yOffset,        DEBUG_CIRCLE_D)
    ellipse(width-xOffset,      height-yOffset,        DEBUG_CIRCLE_D)
    ellipse(width/2-xOffset,    0-yOffset,             DEBUG_CIRCLE_D)
    ellipse(0-xOffset,          height/2-yOffset,      DEBUG_CIRCLE_D)
    ellipse(width/2-xOffset,    height-yOffset,        DEBUG_CIRCLE_D)
    ellipse(width-xOffset,      height/2-yOffset,      DEBUG_CIRCLE_D)

    // Display screen number
    let s = `SCREEN (${displayNo[0]}, ${displayNo[1]})`
    textSize(32)
    stroke(0)
    strokeWeight(4)
    fill(255)
    text(s, 50-xOffset, 50-yOffset)
  }
}


// OPTIONAL FUNCTION
// Placed in setup() after galaxySetup()
// Stops all execution for a short time and synchronises each
// sketch on the next multiple of 10 seconds.
function galaxySyncStart() {
  // In case galaxyReady is true, set back to false
  galaxyReady = false
  // Redisplay the status message if hidden
  status_msg.style("display", "block")
  // Only if you aren't waiting for network config
  if (!wsrEndpoint) {
    galaxyTimedEvent(doGalaxyReady, NET_TIMEOUT)
  }
}


// Function that can be used to time events accross screens.
// Simply executes an event at the next m-milisecond interval.
// Eg, m=10000, the event will be called at the next 10th second
// All displays must call this function within the m interval for this
// function to work accurately.
function galaxyTimedEvent(event, m) {
  // Get the current time
  let t = getUnixMilli()
  // Round up to the next m-seconds
  let rt = ceil(t/m)*m
  // Get the difference
  t = (rt - t)
  // Set the function to execute on time
  setTimeout(event, t)
  return rt
}


// Sends data over the WebSocket relay, ensuring that it is in JSON format
function galaxyBroadcast(data) {
  if (wsrEndpoint) {
    if (typeof data != "string") {
      data = JSON.stringify(data)
    }
    relay.send(data)
  }
}


function getUnix() {
  let d = new Date()
  return floor(d.getTime()/1000)
}


function getUnixMilli() {
  let d = new Date()
  return d.getTime()
}






// Reserved library functions (not to be used publically) //
function doGalaxyReady() {
  // Prevent ready from being called more than once
  if (!galaxyReady) {
    // Hide the scrollbars
    document.body.style.overflow = "hidden"
    // Fix margins for left and top
    document.body.style.marginLeft = -MARGIN
    document.body.style.marginTop = -MARGIN
    // Hide the status message
    status_msg.hide()
    // Initialise the offsets
    updateOffsets()
    // Initialise the real width/height variables
    updateDimensions()
    // Calculate the ID by turning it into an index in a 1D array
    displayID = (numScreensX) * displayNo[1] + displayNo[0]
    // +10 for canvas margin changes above
    createCanvas(screenW + MARGIN, screenH + MARGIN)

    // Set the start time
    startTime = getUnixMilli()
    // Flag galaxy as ready
    galaxyReady = true
    // If the onready funtion exists, use it
    if (onGalaxyReady) {
      onGalaxyReady()
    }
  }
}


// Calculate the translation offsets
// This process assumes all screens are exactly the same
function updateOffsets() {
  xOffset = -displayNo[0] * (screenW + 2*bezel)
  yOffset = -displayNo[1] * (screenH + 2*bezel)
}


function updateDimensions() {
  realWidth = numScreensX*(screenW + 2*bezel)
  realHeight = numScreensY*(screenH + 2*bezel)
}


// Gets the random seed from all nodes
function getRandSeed() {
  console.log("Negotiating with displays to get random seed...")
  // Load the seed for this computer
  seeds.push(floor(random(getUnix())))
  //console.log(seeds)
  relay.onmessage = randomSeedListener

  // Start chaining broadcasts of this computer's seed
  broadcastSeed()

  // Call the finishing function after the timeout
  setTimeout(randSeedDone, NET_TIMEOUT)
}


// Listens for messages for the random seed
function randomSeedListener(event) {
  let data = Number(event.data)
  // If it is a unique number, then add it to the seeds
  if (data && !(data in seeds)) {
    // If there are now enough seeds to start, finish the process
    if (seeds.push(data) >= totalScreens) {
      randSeedDone()
    }
  }
}


function broadcastSeed() {
  // Only broadcast if the network isn't configured and we are listening for seed config
  if (!netReady && relay.onmessage) {
    console.log("message broadcasted")
    relay.send(seeds[0])
    setTimeout(broadcastSeed, 250)
  }
}


function randSeedDone() {
  // If the seed is not set
  // (statement to prevent the NET_TIMEOUT from calling this twice)
  if (!galaxyReady) {
    // If all the screens broadcasted
    if (seeds.length >= totalScreens) {
      seed = min(seeds)
      randomSeed(seed)
      console.log(`Random seed negotiated: ${seed}`)
      relay.onmessage = onRelayMsg || DEFAULT_RELAY_CALLBACK
      // Keep broadcasting the seed for a little while so that slower
      // displays can catch up
      setTimeout(() => {netReady = true}, NET_TIMEOUT/2)
      // Ready the galaxylib on a time interval
      galaxyTimedEvent(doGalaxyReady(), NET_TIMEOUT)

    // Otherwise not all screens broadcasted
    } else {
      galaxyNetError("Failed to negotiate random seed, one of the displays may be offline")
    }
  }
}


function relayOpen() {
  console.log("Connection to " + wsrEndpoint + " has been established.")
  if (debug) {
    console.log("In debug mode, closing connection...")
    relay.close()
    // Continue to run debugging
    doGalaxyReady()
    return
  }
  getRandSeed()
}


function relayError(event) {
  console.log("Websocket relay experienced an error: ", event)
}


function galaxyNetError(msg) {
  // If the onerror callback calls this, change it to a string
  if (msg.type == 'error') {
    msg = "WebSocket connection failed"
  }
  // Don't react to any incoming messages
  relay.onmessage = null
  // Make sure the sketch is stopped
  noLoop()
  // Print the error message
  console.error(msg)
  // Make the status message visible
  status_msg.style("display", "block")
  status_msg.html(msg)
}
