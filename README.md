# p5js-galaxy

## URL Arguments

Example: `http://myserver.com/sketch.html?dx=0&screensx=2&endpoint=http://myrelay.com/`

* `w` : Width of the screens (px). This is the size of the canvas that will appear on the screen and it is assumed that all screens have the same width. DEFAULT: window width.

* `h` : Same as above except with height

* `screensx` : The number of screens in the x-direction, required when using multiple displays (1 to N). DEFAULT: 1

* `screensy` : The number of screens in the y-direction, required when using multiple displays (1 to N). DEFAULT: 1

* `dx` : The x-index for the current display (0 to N-1). DEFAULT: 0

* `dy` : The y-index for the current display (0 to N-1). DEFAULT: 0

* `bezel` : The amount of bezel to draw on each side of each screen (px). DEFAULT: 0

* `debug` : Whether to run the debugging graphics (true/false). DEFAULT: false

* `endpoint` : The WebSocket relay endpoint to enable networking functionality.

## Using the library

The most important variables you will need to use are the following:

* realWidth/realHeight: The width and height of the total canvas which includes all the screens.

* xOffset/yOffset: The current screen's offset relative to all the other screens. You can see this being used in the translate() function below.

Here is an example template with commented explanations as to how you can use different parts of the library.

```
function setup() {
  // Always call this setup function when using the library
  galaxySetup()

  // If you want your sketch on each screen to start at the same
  // time, call this function.
  galaxySyncStart()

  onGalaxyReady = () => {
    /*
    When the library is ready setting up,
    it will call this function.
    */
  }
}


function draw() {
  // Use this if-statement to ensure your code will only run
  // when the library is ready.
  if (galaxyReady) {
    // If you want your screens to represent their own space,
    // use this translate function so that the current screen
    // is correctly positioned relative to the other screens.
    translate(xOffset, yOffset)

    // YOUR DRAW CODE HERE //

    // If you want to allow for debugging mode, place
    // this function at the end of your draw function.
    // Remember to set debug=true in the url parameters.
    galaxyDebug()
  }
}


// Example, broadcast a packet over the network when mousepressed:
// Requires endpoint be specified in url parameters.
function mousePressed() {
  // This function takes in JSON objects or stringified JSON.
  // It will broadcast this JSON to all other connected screens.
  galaxyBroadcast({'field' : true})
}


// This function overrides the variable in the library.
// Its purpose is to handle incoming JSON messages from other screens.
function onRelayMsg(event) {
  data = JSON.parse(event.data)
  if (data.field) {
    // Do something
  }
}
```
